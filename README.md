# openGauss

## 编译项目

为了生成openEuler-riscv64上的RPM包，建议使用openEuler的容器进行构建。

x86_64 可以使用 qemu + docker 的方式运行容器
SG2042 可以直接使用 docker 的方式运行容器

### 配置QEMU（SG2042无需执行这一步）

```
docker run --rm -it --privileged xfan1024/qemu-user-static
```

### 启动openEuler容器

```
# 这一步需要在该项目目录下执行
docker run -it --name oerv-opengauss-build -v$(pwd):/root/rpmbuild/SOURCES xfan1024/openeuler:24.03-riscv64
```

### 进入容器后配置编译环境

```
cd /root/rpmbuild/SOURCES

# 安装必要工具
dnf install -y rpm-build rpmdevtools dnf-plugins-core
# 安装编译依赖
yum-builddep -y opengauss-server.spec
# 下载源码
spectool -g opengauss-server.spec
```

### 编译RPM包

```
rpmbuild -ba opengauss-server.spec
```

## 安装测试

### 安装openGauss数据库

```bash
# 编译本项目后，安装产生的rpm包
dnf install -y opengauss-5.1.0.rpm
systemctl enable --now opengauss-server
```

### 创建数据库和用户

```bash
# 切换至 opengauss 用户
su opengauss

# 连接数据库
gsql -d postgres
```

当gsql连接数据库成功后，在gsql交互界面中输入

```sql
alter role "opengauss" password "openGauss@2024"; -- 修改默认用户密码
create user xiaofan identified by 'xiaofan@2024' profile default;  -- 创建用户 xiaofan
alter user xiaofan sysadmin;
create database xfdb encoding 'UTF8' template=template0 owner xiaofan; -- 创建数据库 
```

以上执行成功后，按`Ctrl+D`退出界面。接下来设置远程登录



### 配置远程登录

```bash
vim /var/lib/opengauss/data/postgresql.conf
# 配置 listen_addresses = '*'
vim /var/lib/opengauss/data/pg_hba.conf
# 末尾增加 host     all            xiaofan         0.0.0.0/0               sha256

gs_ctl -D $HOME/data reload
# reload后即可生效
```

### 本地测试

使用 `gsql -U xiaofan -d xfdb` 连接数据库，创建表，并作简单的增删查操作

```sql
create table phonebook (
    id serial primary key,
    name varchar(20),
    phone varchar(20)
);

insert into phonebook (name, phone) values ('工商银行', '95588');
insert into phonebook (name, phone) values ('招商银行', '95555');
insert into phonebook (name, phone) values ('农业银行', '95599');

insert into phonebook (name, phone) values ('邮政快递', '11183');
insert into phonebook (name, phone) values ('顺丰快递', '95338');
insert into phonebook (name, phone) values ('京东物流', '95311');

select * from phonebook where name like '%银行';
select count(*) from phonebook;
delete from phonebook where name = '农业银行';
select * from phonebook;

```

### 远程测试

远程可以使用 dbeaver 连接数据库，连接时使用 xiaofan/xiaofan@2024 连接即可

注意：使用dbeaver可能需要手动设置openGauss的数据库驱动，可以从 [openGauss-5.0.3-JDBC.tar.gz](https://opengauss.obs.cn-south-1.myhuaweicloud.com/5.0.3/arm_2203/openGauss-5.0.3-JDBC.tar.gz) 获得

![dbeaver](dbeaver-screenshot.png)
