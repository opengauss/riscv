#!/bin/bash
#
# Copyright (c) 2024 Institute of Software, CAS.
# Author: Tan Xiaofan <xiaofan@iscas.ac.cn>
#

datadir="$HOME/data"
datatmp="$HOME/.data.tmp"

if [ ! -e "$datadir" ]; then
     rm -rf "$datatmp"
     #使用gs_initdb初始化用户为opengauss的数据库
     /opt/opengauss/bin/gs_initdb -D "$datatmp" -U opengauss --nodename=single_node
     mv "$datatmp" "$datadir"
fi