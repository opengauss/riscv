%global __brp_check_rpaths %{nil}
%global debug_package %{nil}
%global apppath /opt/opengauss
%global datapath /var/lib/opengauss
%global buildroot_apppath %{buildroot}%{apppath}
%global buildroot_datapath %{buildroot}%{datapath}

Name:           opengauss-server
Version:        6.0.0
Release:        2%{?dist}
Summary:        openGauss is an open source relational database management system
License:        MulanPSL-2.0 and MIT and BSD and zlib and TCL and Apache-2.0 and BSL-1.0
URL:            https://gitee.com/opengauss/openGauss-server
# https://gitee.com/opengauss/openGauss-server/repository/archive/v6.0.0.tar.gz
Source0:        openGauss-server-v6.0.0.tar.gz
Source2:        copysharedlibs.sh
Source10:       opengauss-server.service
Source11:       init-opengaussdb.sh
Source12:       opengauss-shell.sh
# https://gitee.com/opengauss/DCF/repository/archive/v5.1.0.tar.gz
Source20:       DCF-5.1.0.tar.gz
# https://gitee.com/opengauss/openGauss-third_party/raw/6.0.0/dependency/aws-sdk-cpp/aws-sdk-cpp-1.11.327.tar.gz
Source21:       aws-sdk-cpp-1.11.327.tar.gz
# https://gitee.com/opengauss/openGauss-third_party/raw/6.0.0/dependency/xgboost/xgboost-v1.4.1.tar.gz
Source22:       xgboost-v1.4.1.tar.gz
# https://gitee.com/opengauss/openGauss-third_party/raw/6.0.0/dependency/dmlc-core/dmlc-core-v0.5.tar.gz
Source23:       dmlc-core-v0.5.tar.gz
Patch1:         using-system-package-instead-binarylibs.patch
Patch2:         integrate-3rd-source-code.patch
Patch3:         Fix-pointer-comparison-syntax-error.patch
Patch4:         link-gaussdb-with-atomic.patch
Patch5:         Using-sm2-curve-generate-key-pair.patch
Patch6:         add-riscv64-support.patch
Patch100:       add-riscv64-support-on-DCF.patch
Patch101:       add-compile-options-to-xgboost.patch

# fedora
Patch200:       include-cstdint.patch
Patch201:       remove-extern-gettimeofday.patch
Patch202:       defer-makeTypeNameFromOid.patch
Patch203:       include-zlib.h.patch
Patch204:       force-link-boundscheck.patch
Patch205:       disable-sm4-crypto.patch

BuildRequires:  automake bison boost-devel cjson-devel cmake dnf-plugins-core flex gcc gcc-c++ git glibc-devel
BuildRequires:  java-1.8.0-openjdk-devel libaio-devel libboundscheck libcgroup-devel libcurl-devel libffi-devel
BuildRequires:  libtool libzstd-devel make ncurses-devel pam-devel patch
BuildRequires:  perl python3-devel python3-pip python3-pyyaml readline-devel tar unzip
BuildRequires:  minizip-devel lz4-devel unixODBC-devel libedit-devel jemalloc-devel krb5-devel libatomic
BuildRequires:  zlib-devel libcurl-devel
BuildRequires:  patchelf vim-common
%if 0%{?openEuler}
BuildRequires:  python3-unversioned-command
BuildRequires:  libtool-devel openeuler-lsb openssl-devel
%else
BuildRequires:  python-unversioned-command
BuildRequires:  libtool-ltdl-devel openssl-devel-engine
%endif 

Requires:       shadow-utils

%description
%{summary}

%prep
%setup -q -n openGauss-server-v%{version}

mkdir -p 3rd/DCF         && tar xf %{SOURCE20} --strip-components=1 -C 3rd/DCF
mkdir -p 3rd/aws-sdk-cpp && tar xf %{SOURCE21} --strip-components=1 -C 3rd/aws-sdk-cpp
mkdir -p 3rd/xgboost     && tar xf %{SOURCE22} --strip-components=1 -C 3rd/xgboost
tar xf %{SOURCE23} --strip-components=1 -C 3rd/xgboost/dmlc-core
sed -i 's/uname -p/uname -m/g' CMakeLists.txt

%patch 1 -p1
%patch 2 -p1
%patch 3 -p1
%patch 4 -p1
%patch 5 -p1
%patch 6 -p1
%patch 100 -p1 -d 3rd/DCF
%patch 101 -p1 -d 3rd/xgboost

# fedora
%if 0%{?fedora}
%patch 200 -p1
%patch 201 -p1
%patch 202 -p1
%patch 203 -p1
%patch 204 -p1
%patch 205 -p1
%endif

%build
%if 0%{?fedora}
export CFLAGS="-O2 -flto=auto -ffat-lto-objects -fexceptions -g -grecord-gcc-switches -pipe -Wall -Wp,-U_FORTIFY_SOURCE,-D_FORTIFY_SOURCE=3 -Wp,-D_GLIBCXX_ASSERTIONS -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -fstack-protector-strong -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1  -fasynchronous-unwind-tables -fno-omit-frame-pointer -mno-omit-leaf-frame-pointer"
export CXXFLAGS="-O2 -flto=auto -ffat-lto-objects -fexceptions -g -grecord-gcc-switches -pipe -Wall -Wp,-U_FORTIFY_SOURCE,-D_FORTIFY_SOURCE=3 -Wp,-D_GLIBCXX_ASSERTIONS -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -fstack-protector-strong -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1  -fasynchronous-unwind-tables -fno-omit-frame-pointer -mno-omit-leaf-frame-pointer"
%endif

mkdir tmp-build
cd tmp-build
export DEBUG_TYPE=release
export ENABLE_LITE_MODE=ON
export PREFIX_HOME=$(pwd)/opengauss_install_dir
cmake .. -DENABLE_MULTIPLE_NODES=OFF -DENABLE_PRIVATEGAUSS=OFF -DENABLE_THREAD_SAFETY=ON \
%if 0%{openEuler}
         -DENABLE_OPENEULER_MAJOR=ON -DWITH_OPENEULER_OS=ON \
%endif
         -DENABLE_LITE_MODE=ON -DENABLE_BBOX=OFF -DENABLE_BBOX=OFF -DTEST=OFF -DCMAKE_BUILD_TYPE=Release
make %{?_smp_mflags}

%install
export DEBUG_TYPE=release
export ENABLE_LITE_MODE=ON
export PREFIX_HOME=$(pwd)/opengauss_install_dir
cd tmp-build
make %{?_smp_mflags} install

mkdir -p %{buildroot_apppath}
install -m 755 %{SOURCE11} %{buildroot_apppath}/init-opengaussdb.sh
install -m 755 %{SOURCE12} %{buildroot_apppath}/opengauss-shell.sh
cp -a opengauss_install_dir/{bin,etc,share} %{buildroot_apppath}
mkdir -p %{buildroot_apppath}/lib
bash %{SOURCE2} %{buildroot_apppath}/bin opengauss_install_dir/lib %{buildroot_apppath}/lib %{apppath}/lib
mkdir -p %{buildroot_apppath}/lib/postgresql
cp -a opengauss_install_dir/lib/postgresql/*.so %{buildroot_apppath}/lib/postgresql
mkdir -p %{buildroot}/usr/lib/systemd/system
install -m 644 %{SOURCE10} %{buildroot}/usr/lib/systemd/system/opengauss-server.service

%pre
/usr/sbin/useradd -r -m -d %{datapath} -s %{apppath}/opengauss-shell.sh -c "openGauss Server" opengauss >/dev/null 2>&1 || :

%post
%systemd_post opengauss-server.service

%preun
%systemd_preun opengauss-server.service

%postun
%systemd_postun_with_restart opengauss-server.service

%files
%{apppath}
/usr/lib/systemd/system/opengauss-server.service

%changelog
* Tue Jan 21 2025 Songsong Zhang <zhangsongsong@iscas.ac.cn> - 6.0.0-2
- Add support for Fedora

* Mon Nov 4 2024 huangji <huangji@iscas.ac.cn> - 6.0.0-1
- Upgrade version to 6.0.0

* Tue Oct 15 2024 xiaofan <xiaofan@iscas.ac.cn> - 5.1.0-1
- Initial package
