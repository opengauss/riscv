#!/bin/bash
#
# Copyright (c) 2024 Institute of Software, CAS.
# Author : Tan Xiaofan <xiaofan@iscas.ac.cn>
#

export GAUSSHOME=/opt/opengauss
GAUSSHOME_BIN=$GAUSSHOME/bin

if [[ ":$PATH:" != *":$GAUSSHOME_BIN:"* ]]; then
    export PATH=$GAUSSHOME_BIN:$PATH
fi

unset GAUSSHOME_BIN

exec /bin/bash
