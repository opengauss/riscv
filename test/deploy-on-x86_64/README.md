# README

当前目录下的文档[main.md](./main.md)介绍了如何在x86_64平台下使用容器（Docker）完成openGauss Risc-V的编译和部署工作。

[openeuler-docker-opengauss-riscv64](./openeuler-docker-opengauss-riscv64/)则包含使用Docker容器自动化x86_64下openGauss RISC-V的构建流程的一些工作。