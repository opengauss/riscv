# x86_64下使用容器（Docker）完成openGauss Risc-V的编译和部署工作

下述操作步骤仅在ArchLinux amd64平台下测试通过，理论上适用于所有的Linux发行版。

在Windows上的运行结果无法保证。

## 环境要求

- git
- docker
  - 为免发行版上游的Docker版本陈旧，应遵循Docker官方文档安装当前发行版下的Docker Engine
  - https://docs.docker.com/engine/install/
  - 截至2025年1月19日，docker官方的apt源并没有提供Debian 13 Trixie的docker包。使用Debian 13的用户应考虑Debian上游的docker包，或者将/etc/apt/source.list.d/docker.list中的Debian版本由trixie变更为bookworm。
- qemu
  - 应确保qemu-riscv64-static在命令行中可用。
    - 在ArchLinux中，确保包qemu-system-riscv、qemu-user-static已经安装
    - 在Debian中，确保包qemu-system-riscv、qemu-user-static已经安装（该包需要Debian 13 Trixie）

## 操作步骤

仓库https://gitee.com/opengauss/riscv提供了openGauss RISC-V部署所需要的一切必须步骤。以下步骤基于该仓库文档展开。

### 第零步 配置QEMU

> 对于直接使用riscv64架构的机器作为容器宿主机的用户，应考虑跳转至第一步

`multiarch/qemu-user-static`让我们能够通过QEMU和binfmt_misc执行与当前设备体系结构（Architecture）不同的容器。访问其[GitHub仓库](https://github.com/multiarch/qemu-user-static)获得更多信息。

运行下述命令行以使用`multiarch/qemu-user-static`：

```bash
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes 
```

如本步骤因不明原因失败，可尝试关闭本机上运行的域名解析服务（或其他一切监听本机53接口的服务，如`smartdns`、`systemd-resolved`）后再次尝试。待本步骤完成后再打开。

更多信息参见：
- https://forum.level1techs.com/t/failed-to-start-network-default-libvirt-errors-why-how-to-fix/207789

### 第一步 容器配置

我们首先需要将openGauss Risc-V的代码克隆到本地：

```bash
git clone https://gitee.com/opengauss/riscv
```

接着我们需要通过Docker构建一个用于编译该仓库中代码的容器：

```bash
cd riscv # 切换到目标代码仓库的目录
docker run -it --name oerv-opengauss-build -v$(pwd):/root/rpmbuild/SOURCES xfan1024/openeuler:24.03-riscv64
```

以上指令构建了一个名为`oerv-opengauss-build`的容器，该容器以交互模式运行（`-i`），并开启了伪终端（`-t`）。该容器将当前目录（`$(pwd)`）挂载到容器中的`/root/rpmbuild/SOURCES`路径。

如若容器启动时，遇到报错：
```
exec /usr/bin/uname: exec format error
libcontainer: container start initialization failed: exec /usr/bin/uname: exec format error
```
则应重试上述第零步。并在删除容器`oerv-opengauss-build`后重新进行本步骤。

### 第二步 使用伪终端编译rpm文件


如若在执行第一步的指令时，未能进入到容器中。可以通过下述指令进入到容器中：
```bash
docker exec -it oerv-opengauss-build /bin/bash
```

通过下述指令在容器中配置环境：
```bash
cd /root/rpmbuild/SOURCES

# 安装必要工具
dnf install -y rpm-build rpmdevtools dnf-plugins-core
# 安装编译依赖
yum-builddep -y opengauss-server.spec
# 下载源码
spectool -g opengauss-server.spec
```

通过下述指令编译RPM包：
```bash
cd /root/rpmbuild/SOURCES
rpmbuild -ba opengauss-server.spec
```

您可以通过提高Docker默认允许的进程/线程数来提高编译速度。访问[ArchWiki Docker#默认的允许的进程/线程数太少](https://wiki.archlinuxcn.org/wiki/Docker#默认的允许的进程/线程数太少)获取更多信息。

编译得到的文件在路径`/root/rpmbuild/RPMS/riscv64/`下

### 第三步 安装rpm文件

使用下述指令安装编译得到的RPM文件：
```bash
cd /root/rpmbuild/RPMS/riscv64/
dnf install -y $(ls *.rpm)
```

### 第四步 在容器中运行数据库

测试数据库是否正确安装并不需要运行数据库。仅运行`test/test.sh`的用户可以跳转到下文第五步。

由于容器并没有init进程，所以不能使用`systemctl start`的方式启动数据库服务。

注意到，openGauss的安装位置在容器中的`/opt/opengauss`目录下。我们可以手动运行openGauss的脚本来模拟生产环境中服务启动的过程。

```bash
su opengauss # 切换到opengauss账户。该账户的home目录为/var/lib/opengauss
/opt/opengauss/init-opengaussdb.sh # 数据库初始化脚本
/opt/opengauss/bin/gaussdb -D /var/lib/opengauss/data --single_node #以单节点模式运行openGauss数据库
```

### 第五步 运行数据库测试脚本

在root用户下，输入下述指令启动测试脚本：
```bash
/root/rpmbuild/SOURCES/test/test.sh
```

该数据库测试脚本需要数据库没有被创建。如若您已经运行上述第四步中的`init-opengaussdb.sh`脚本进行了数据库的初始化，那么请运行下述指令：

```bash
mv /var/lib/opengauss/data{,.bak}
```
该指令将`/var/lib/opengauss/data`文件夹重命名为`data.bak`。

若您需要**删除当前设备上已经创建的数据库**，那么请运行下述指令：

```
rm -rf /var/lib/opengauss/data
```

