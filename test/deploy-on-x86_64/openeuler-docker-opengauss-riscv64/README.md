# 使用Docker容器自动化x86_64下openGauss RISC-V的构建和测试流程

## 在Docker中构建opengauss/riscv仓库的代码，并导出为rpm文件

下述指令应该由具有docker用户组权限的用户执行，如果执行失败，请尝试在命令前加上`sudo`，或者检查您的docker安装。
```
make only-compile #构建代码
make cpfile #导出rpm文件
```
上述指令执行完毕后，当前目录下会出现文件夹`riscv64`。编译完毕的rpm文件会出现在其中。

## [WIP]构建用于测试的openGauss RISC-V运行时容器
```
make build
docker run -itd --rm <镜像名> -p <宿主机端口>:5432 -v <宿主机持久化目录>:<容器内持久化目录>
```

## Roadmap

- [x] 自动化编译流程
  - [ ] 提供参数以选择上游代码库（目前固定为https://gitee.com/opengauss/riscv的v6.0.0版本）
- [ ] 构建用于测试的openGauss RISC-V运行时容器
  - [ ] 提供entrypoint脚本
  - [ ] 规划从docker run中接收的环境变量
  - [ ] 提供docker compose文件