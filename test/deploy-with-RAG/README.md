# README

当前目录下的文档[main.md](./main.md)介绍了使用openGauss构建RAG的一些尝试性工作。

有关于openGauss+DeepSeek构建RAG在其他架构上的可行性，应参见：[打破AI黑盒，拥抱开源力量：基于openGauss+DeepSeek的本地知识库，打造你的专属AI助手！](https://mp.weixin.qq.com/s/4KPE_dGg-Z8IJZwdfUcXtw)

在risc-v平台上构建ollama的教程应参见Jeff Geerling的博文[How to build Ollama to run LLMs on RISC-V Linux](https://www.jeffgeerling.com/blog/2025/how-build-ollama-run-llms-on-risc-v-linux)