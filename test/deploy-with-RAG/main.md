# 通过openGauss RISC-V + Ollama + DeepSeek R1构建本地RAG知识库
WIP
## 引言：什么是LLM？什么是RAG？

大型语言模型（LLM，Large Language Model） 是一种基于深度学习的自然语言处理模型，旨在理解和生成人类语言。LLM 通过在大规模文本数据上进行训练，学习语言的统计规律和语义结构。其核心架构通常基于 Transformer，该架构通过自注意力机制（Self-Attention）捕捉文本中的长距离依赖关系。

LLM 的训练分为两个主要阶段：

- 预训练：模型在大量无标注文本上学习语言的基本模式，如词汇、语法和上下文关系。
- 微调：模型在特定任务（如问答、翻译等）上进行进一步训练，以优化其性能。

LLM 的典型应用包括文本生成、机器翻译、问答系统等。其优势在于能够处理开放域任务，但局限性在于可能生成不准确或不符合事实的内容。

检索增强生成（RAG，Retrieval-Augmented Generation）是一种结合检索机制和生成模型的技术，旨在提升 LLM 的准确性和可靠性。RAG 的核心思想是通过外部知识库动态检索相关信息，并将其作为上下文输入生成模型，从而生成更准确的回答。

RAG 的优势在于：
- 减少生成模型对训练数据的依赖，避免生成不准确的内容。
- 能够动态利用最新或领域特定的知识，提升回答的时效性和专业性。

通过结合RAG，我们可以结合本地的知识库或者其他工作载荷，快速构建起一个能帮助我们迅速检索信息的AI助手，提高生产生活水平。

## 前置环境部署

*最后一步的rag需要用到数据中的vector类型。该类型依赖于openGauss 7.0.0。本sig组的当前移植的openGauss版本为6.0.0，暂时不满足依赖。*

*openGauss的部署不包括在本节内容，但是我们建议您部署openGauss前阅读如下链接的内容：*

- https://blog.csdn.net/GaussDB/article/details/128091194

*请重点关注该文章3.3节《创建数据库连接用户》，以开启openGauss对MD5方式连接的限制。*

*`否则您可以遇到如下报错：*

```bash
psycopg2.OperationalError: none of the server’s SASL authentication mechanisms are supported
```

---

测试系统为openEuler 24.03 LTS-SP1。

截止至2025年2月25日，ollama仍未支持riscv64架构。我们需要通过源码构建安装ollama。构建和运行ollama以及RAG的工作载荷，所需的环境包括：
- Python3
  - pdm（可选）
  - ollama（pip包）
  - psycopg2（pip包）
- rust工具链
- golang工具链
- libpq-devel

```bash
sudo dnf install -y cmake curl wget python3 libpq-devel # 安装依赖
wget https://go.dev/dl/go1.23.5.linux-riscv64.tar.gz # 安装golang工具链
sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf go1.23.5.linux-riscv64.tar.gz
export PATH=$PATH:/usr/local/go/bin # 将golang工具链的bin目录加入到PATH中
echo "export PATH=$PATH:/usr/local/go/bin">>~/.profile

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh # 通过rustup安装rust工具链

curl -sSL https://pdm-project.org/install-pdm.py | python3 - # 安装pdm，作为venv环境的管理器
export PATH=$PATH:$HOME/.local/bin # 将pdm二进制所处位置的路径加入到PATH中
echo "export PATH=$PATH:$HOME/.local/bin">>~/.profile

mkdir ograg #创建临时工作目录，我们所有的工作都将在该目录中完成
pdm init # 按照提示创建python项目——一路回车就行了
eval `pdm venv activate in-project` # 启动项目虚拟环境
pdm add ollama psycopg2 # 安装必须的python包

# 构建ollama
git clone --recurse-submodules https://github.com/mengzhuo/ollama.git
cd ollama
go build .
sudo ln -s `pwd`/ollama /usr/local/bin/ollama

ollama serve & # 启动ollama服务
ollama pull deepseek-r1 # 拉取DeepSeek模型
ollama pull nomic-embed-text # 拉取nomic-embed-text用于嵌入任务
```

测试psycopg2环境安装是否成功：

```python
# 测试是否可以正常使用psycopg2连接上openGauss
import psycopg2

conn = psycopg2.connect(
    database="postgres", # 将此替换为您想要连接上的数据库
    user="gaussdb", # 将此替换为您数据库的用户名
    password="Test@123", # 将此替换为您数据库的密码
    host="127.0.0.1", # 将此替换为您数据库的域名/IP
    port="8888" # 将此替换为您数据库的端口
    # 若您尝试连接部署在公网的openGauss数据库，请阅读psycopg2文档，了解如何通过SSL方式连接数据库，以减少潜在的数据安全风险
)

cur = conn.cursor()
cur.execute("select version();")
rows = cur.fetchall()
print(rows)
```

# 测试运行

先通过shell下载一些文档作为语料：
```bash
# 下载opengauss的文档作为语料
wget https://gitee.com/opengauss/website/raw/v2/app/zh/faq/index.md
```

完整代码：

```python
# 语料嵌入
import psycopg2
import ollama

conn = psycopg2.connect(
    database="postgres", # 将此替换为您想要连接上的数据库
    user="gaussdb", # 将此替换为您数据库的用户名
    password="Test@123", # 将此替换为您数据库的密码
    host="127.0.0.1", # 将此替换为您数据库的域名/IP
    port="8888" # 将此替换为您数据库的端口
    # 若您尝试连接部署在公网的openGauss数据库，请阅读psycopg2文档，了解如何通过SSL方式连接数据库，以减少潜在的数据安全风险
)

#验证连接
cur = conn.cursor()
cur.execute("select version();")
rows = cur.fetchall()
print(rows)

file_path = '/tmp/index.md' # 语料存储的路径

with open(file_path, 'r', encoding='utf-8') as file:
    content = file.read()

paragraphs = content.split('##')

for i, paragraph in enumerate(paragraphs):
    print(f'段落 {i + 1}:\n{paragraph}\n')
    print('-' * 20)

def embedding(text):
    vector = ollama.embeddings(model="nomic-embed-text", prompt=text)
    return vector["embedding"]

text = "openGauss 是一款开源数据库"
emb = embedding(text)
dimensions = len(emb)
print("text : {}, embedding dim : {}, enbedding : {} ...".format(text, dimensions, emb[:10]))

table_name = "opengauss_data"
cur = conn.cursor()
cur.execute("DROP TABLE IF EXISTS {};".format(table_name))
cur.execute("CREATE TABLE {} (id INT PRIMARY KEY, content TEXT, emb vector({}));".format(table_name, dimensions))
conn.commit()

# 插入数据
for i, paragraph in enumerate(paragraphs):
    emb = embedding(paragraph)
    insert_data_sql = f'''INSERT INTO {table_name} (id, content, emb) VALUES (%s, %s, %s);'''
    cur.execute(insert_data_sql, (i, paragraph, emb))
conn.commit()

# 创建索引
cur.execute("CREATE INDEX ON {} USING hnsw (emb vector_l2_ops);".format(table_name))
conn.commit()

# 提问

question = "openGauss 发布了哪些版本？"

emb_data = embedding(question)
dimensions = len(emb_data)

cur = conn.cursor()
cur.execute("select content from {} order by emb <-> '{}' limit 1;".format(table_name, emb_data))
conn.commit()

rows = cur.fetchall()
print(rows)

cur.close()
conn.close()
```

<!-- TODO:上述脚本在实例上跑出来有问题
Traceback (most recent call last):
  File "/home/wold9168/og-rag/importdata.py", line 41, in <module>
    cur.execute("CREATE TABLE {} (id INT PRIMARY KEY, content TEXT, emb vector({}));".format(table_name, dimensions))
psycopg2.errors.UndefinedObject: type "vector" does not exist
LINE 1: ...gauss_data (id INT PRIMARY KEY, content TEXT, emb vector(768...

应该是数据库不支持vector类型导致的，应该考虑开启数据库的向量支持，或者考虑https://github.com/pgvector/pgvector的移植。
-->