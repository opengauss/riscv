
**注意**

本文件夹下的针对 openGauss 的测试步骤及测试报告来源于 PLCT 丁丑小队在 Milk-V Pioneer Box 和 Sipeed LicheePi 4A 上做的测试工作！

原链接：[openGauss专项测试报告](https://github.com/QA-Team-lo/dbtest/tree/0e7e65b4f01a0aea8d7f423ee25478f31e24c038/report/opengauss)

原链接：[安装openGauss数据库](https://github.com/QA-Team-lo/dbtest/blob/0e7e65b4f01a0aea8d7f423ee25478f31e24c038/opengauss/install.md)

原链接：[测试 openGauss 数据库](https://github.com/QA-Team-lo/dbtest/blob/0e7e65b4f01a0aea8d7f423ee25478f31e24c038/opengauss/test.md)

原链接遵循: [Apache License Version 2.0协议](https://www.apache.org/licenses/LICENSE-2.0)