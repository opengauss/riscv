\set ON_ERROR_STOP on
alter role "opengauss" password "openGauss@2024";   -- opengauss要求修改的密码不能和原密码一致
create user xiaofan identified by 'xiaofan@2024' profile default;  -- 创建用户 xiaofan
alter user xiaofan sysadmin;
create database xfdb encoding 'UTF8' template=template0 owner xiaofan; -- 创建数据库 
