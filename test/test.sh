#!/bin/bash

# 清理函数，用于删除数据库数据目录并强制杀死所有gaussdb进程
tst_cleanup(){
    rm -rf /var/lib/opengauss/data  # 删除数据库数据目录
    killall -9 gaussdb 2>/dev/null  # 强制终止所有gaussdb进程
}

# 错误处理函数，发生错误时清理并退出
tst_onerror(){
    echo "occur error: $*" >&2
    tst_cleanup
    exit 1
}

# 命令执行函数，执行命令并在失败时调用错误处理
tst_cmd(){
    if ! eval "$*"; then
        tst_onerror "$@"
    fi
}

# 定义通用的openGauss命令执行函数
gs_cmd(){
    sudo -u opengauss GAUSSHOME=/var/lib/opengauss "$@"
}

# 执行openGauss SQL语句的辅助函数
gs_sql(){
    gs_cmd /opt/opengauss/bin/gsql -d postgres "$@"
}

# 控制openGauss服务的辅助函数
gs_ctl(){
    gs_cmd /opt/opengauss/bin/gs_ctl -D /var/lib/opengauss/data "$@"
}

# 切换到脚本所在目录
cd "$(dirname "$0")"

# 如果数据目录已经存在，则报错并退出
if [ -e "/var/lib/opengauss/data" ]; then
    echo "Error: database already exists" >&2
    exit 1
fi

# 初始化openGauss数据库
tst_cmd 'gs_cmd /opt/opengauss/init-opengaussdb.sh'

# 启动数据库服务
tst_cmd 'gs_ctl start'

# 执行SQL文件测试
tst_cmd 'gs_sql -f sql/create-user-db.sql'
tst_cmd 'gs_sql -f sql/test-phonebook.sql'

# 验证电话簿表中记录的数量为5
tst_cmd 'test $(gs_sql -A -t -c "select count(*) from phonebook;") = "5"'
tst_cmd 'gs_sql -c "delete from phonebook where name like '"'"'%银行'"'"';"'

# 停止数据库服务
tst_cmd 'gs_ctl stop'

# 重新启动数据库服务
tst_cmd 'gs_ctl start'

# 验证电话簿表中记录的数量为3
tst_cmd 'test $(gs_sql -A -t -c "select count(*) from phonebook;") = "3"'

# 停止数据库服务
tst_cmd 'gs_ctl stop'

# 清理测试环境
tst_cleanup

# 输出测试成功信息
echo "Test openGauss success"
